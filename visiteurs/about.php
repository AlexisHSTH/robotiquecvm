
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>À propos de nous - Les Vieux-Robots</title>
	<link href="css/global.css" rel="stylesheet" type="text/css" />
	<link href="css/nav.css" rel="stylesheet" type="text/css" />
	<link href="css/about.css" rel="stylesheet" type="text/css" />
	<link href="css/form.css" rel="stylesheet" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>


<?php

	// ALLER CHERCHER LE NAV DU SITE
	require_once("partial/nav.php");

 ?>

 <div class="buildspace"></div>

		<!-- <header>
			<div class="buildspace"></div>
			<h1>À propos de nous</h1>
		</header> -->

		<main>

			<!-- <div id="group1">
			<div class="banner">
				<h1>Qui sommes nous</h1>
				<p>Nous représentons le Cégep du Vieux-Montréal dans la compétition de Robotique.</p>
				<p>Visitez notre site pour obtenir plus d'informations sur l'équipe et les événements à venir.</p>
			</div>

			<div id="confiance" class="banner">
				<h1>L'idée originale</h1>
				<p>Utiliser cette section pour y insérer des commentaires sur ce qui inspire l'équipe.</p>
				<p>Il peut être une bonne idée de parler de ce qui a motivé chacun à former ce groupe et des raisons pour lesquelles on poursuit cet objectif.</p>

			</div>

			</div> -->




			<div class="banner">
				<h1>Communiquer avec nous</h1>
				<p>Vous pouvez toujours nous écrire directement si vous avez des questions ou si vous avez un message à nous transmettre. Il nous fera un plaisir de vous répondre.</p>

			<form id="contact" action="sendtomail.php?requestType = 'contact'", method="post", onsubmit="return validate()">
				<input id="requestType" type="hidden" name="requestType" value="contact">
				<div class="line">
					<div class="lab">Prénom* : </div>
				</div>
				<div class="line">
					<div class="field"><input id="prenom" type="text" name="prenom" required></div>
				</div>
				<div class="line">
					<div class="lab">Nom* : </div>
				</div>
				<div class="line">
					<div class="field"><input id="nom" type="text" name="nom" required></div>
				</div>
				<div class="line">
					<div class="lab">Courriel* : </div>
				</div>
				<div class="line">
					<div class="field"><input id="email" type="text" name="email" required></div>
				</div>

				<div class="line">
					<div class="lab">Numéro de téléphone : </div>
				</div>
				<div class="line">
					<div class="field"><input id="phone" type="text" name="phone"></div>
				</div>
				<div class="line">
					<div class="lab">Message*</div>
				</div>
				<div class="line">
					<div class="field"><textarea id="message" type="text" rows:20 name="message" required></textarea></div>
				</div>

				<div class="line">
					<div class="lab"></div>
				</div>
				<div class="line">
					<div class="field"><input class="submit" type="submit" value="Envoyer"></div>
				</div>



			</form>

			</div>



		</main>

		<?php

			// ALLER CHERCHER LE CONTENU DE L'EN-TÊTE DU SITE
			require_once("partial/footer.php");

		 ?>
