<?php
	require_once("action/DAO/Connection.php");

	class UserDAO {

		// Aller chercher les réponses dans la DB
		public static function fetchAnswers() {
			$connection = Connection::getConnection();
			// echo "connected";
			//echo $connection;

			// Plante à partir d'ici.
			$statement = $connection->prepare("SELECT * FROM membres");
			// echo "stated";

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			// echo "executed statement";

			$results = $statement->fetchAll();



			//$results = 0;
			// echo($results[0]['nom']);

			return $results;

		}



	}
