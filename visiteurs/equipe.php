<?php
	mb_internal_encoding('UTF-8');
	require_once("action/MembresAction.php");

	$action = new MembresAction();
	$action->executeAction();

	$membres = $action->membres;

?>

<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>L'Équipe - Les Vieux-Robots</title>
	<link href="css/global.css" rel="stylesheet" type="text/css" />
	<link href="css/nav.css" rel="stylesheet" type="text/css" />
	<link href="css/galerie.css" rel="stylesheet" type="text/css" />
	<link href="css/equipe.css" rel="stylesheet" type="text/css">
	<link href="css/membre.css" rel="stylesheet" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>


<?php

	// ALLER CHERCHER LE NAV DU SITE
	require_once("partial/nav.php");

 ?>


		<header>
			<div class="buildspace"></div>
			<h1>L'Équipe</h1>
		</header>

		<main>

			<p>Voici les membres de notre équipe. Peut-être en reconnaîtrez-vous quelques-uns, qui sait.</p>
			<!-- <h2>Médias</h2>
			<h2>Conception physique</h2>
			<h2>Programmation</h2> -->






			<section id="team" class="section">



					<?php
  		foreach($membres as $m){
 			if($m["fonction"] != "Programmation" && $m["fonction"] != "Mécanique" && $m["fonction"] != "Média"){
  		?>

 			<div class="membre">

 				<?php
 				if(is_null($m["photo"])){ ?>
 						<!-- <img src="images/team/facedecon.jpg"> -->
 						<img class="photo" src="images/team/PersonGray.png">
 				<?php }
 				else{
 					echo '<img class="photo" src="data:image/jpeg;base64,'.base64_encode( $m['photo'] ).'" style="border-radius: 50%"/>';

 					?>

 				<?php } ?>
 				<h3><?= $m["nom"] ?></h3>

 				<div class="membre-overlay"> <!-- style="background-color:<?= $m['couleur'] ?>;"> -->

 					<table>
 						<tr>
 							<td><img src="images/team/JobWhite.png"></td>
 							<td><p><?= $m["programme"] ?></p></td></tr>
 						<tr>
 							<td><img src="images/team/JobWhite.png"></td>
 							<td><p><?= $m["fonction"] ?></p></td></tr>

 						<tr>
 							<td><img src="images/team/CheckWhite.png"></td>
 							<td><p><?= $m["apprentissage"] ?></p></td></tr>
 					</table>
 				</div>

 			</div>
 		<?php
 			}
 		}
 		?>



 		<h2>Médias</h2>

 		<?php
 		foreach($membres as $m){

 			if($m["fonction"] == "Média"){

 		?>


 			<div class="membre">

 				<?php
 				if(is_null($m["photo"])){ ?>
 						<!-- <img src="images/team/facedecon.jpg"> -->
 						<img class="photo" src="images/team/PersonGray.png">
 				<?php }
 				else{
 					echo '<img class="photo" src="data:image/jpeg;base64,'.base64_encode( $m['photo'] ).'" style="border-radius: 50%"/>';

 					?>

 				<?php } ?>
 				<h3><?= $m["nom"] ?></h3>

 				<div class="membre-overlay"> <!-- style="background-color:<?= $m['couleur'] ?>;"> -->

 					<table>
 						<tr>
 							<td><img src="images/team/JobWhite.png"></td>
 							<td><p><?= $m["programme"] ?></p></td></tr>
 						<tr>
 							<td><img src="images/team/JobWhite.png"></td>
 							<td><p><?= $m["fonction"] ?></p></td></tr>

 						<tr>
 							<td><img src="images/team/CheckWhite.png"></td>
 							<td><p><?= $m["apprentissage"] ?></p></td></tr>
 					</table>
 				</div>

 			</div>
 		<?php
 			}
 		}
 		?>

 		<h2>Conception physique</h2>

 		<?php
 		foreach($membres as $m){
 			if($m["fonction"] == "Mécanique"){


 		?>

 			<div class="membre">

 				<?php
 				if(is_null($m["photo"])){ ?>
 						<!-- <img src="images/team/facedecon.jpg"> -->
 						<img class="photo" src="images/team/PersonGray.png">
 				<?php }
 				else{
 					echo '<img class="photo" src="data:image/jpeg;base64,'.base64_encode( $m['photo'] ).'" style="border-radius: 50%"/>';

 					?>

 				<?php } ?>
 				<h3><?= $m["nom"] ?></h3>

 				<div class="membre-overlay"> <!-- style="background-color:<?= $m['couleur'] ?>;"> -->

 					<table>
 						<tr>
 							<td><img src="images/team/JobWhite.png"></td>
 							<td><p><?= $m["programme"] ?></p></td></tr>
 						<tr>
 							<td><img src="images/team/JobWhite.png"></td>
 							<td><p><?= $m["fonction"] ?></p></td></tr>

 						<tr>
 							<td><img src="images/team/CheckWhite.png"></td>
 							<td><p><?= $m["apprentissage"] ?></p></td></tr>
 					</table>
 				</div>

 			</div>
 		<?php
 			}
 		}
 		?>

 		<h2>Programmation</h2>

 		<?php
 		foreach($membres as $m){
 			if($m["fonction"] == "Programmation"){
 		?>

 			<div class="membre">

 				<?php
 				if(is_null($m["photo"])){ ?>
 						<!-- <img src="images/team/facedecon.jpg"> -->
 						<img class="photo" src="images/team/PersonGray.png">
 				<?php }
 				else{
 					echo '<img class="photo" src="data:image/jpeg;base64,'.base64_encode( $m['photo'] ).'" style="border-radius: 50%"/>';

 					?>

 				<?php } ?>
 				<h3><?= $m["nom"] ?></h3>

 				<div class="membre-overlay"> <!-- style="background-color:<?= $m['couleur'] ?>;"> -->

 					<table>
 						<tr>
 							<td><img src="images/team/JobWhite.png"></td>
 							<td><p><?= $m["programme"] ?></p></td></tr>
 						<tr>
 							<td><img src="images/team/JobWhite.png"></td>
 							<td><p><?= $m["fonction"] ?></p></td></tr>

 						<tr>
 							<td><img src="images/team/CheckWhite.png"></td>
 							<td><p><?= $m["apprentissage"] ?></p></td></tr>
 					</table>
 				</div>

 			</div>
 		<?php
 			}
 		}
 		?>




			</section>



		</main>

		<?php

			// ALLER CHERCHER LE CONTENU DE L'EN-TÊTE DU SITE
			require_once("partial/footer.php");

		 ?>
