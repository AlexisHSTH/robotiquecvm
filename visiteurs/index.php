<html>
<head>

  <meta charset="utf-8">
  <title>Les Vieux-Robots</title>
  <link href="css/global.css" rel="stylesheet" type="text/css">
  <link href="css/nav.css" rel="stylesheet" type="text/css">
  <link href="css/home.css" rel="stylesheet" type="text/css">
  <link href="css/newslist.css" rel="stylesheet" type="text/css">
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>

  <?php

  	// ALLER CHERCHER LA BARRE DE NAVIGATION
  	require_once("partial/nav.php");
	// echo("Je viens d'un code PHP!!!!");

   ?>

  <header>
	  <div class="buildspace"></div>
    <video autoplay muted loop id="header-video">
      <source src="videos/vieuxrobots.mp4" type="video/mp4">
    </video>
	  <h1 class="header-content">Les Vieux-Robots<br/>Cégep du Vieux-Montréal</h1>
    <img class="header-content" id="Title-Logo" src="images/icon.png" height=150px width=150px/>
    <a href="#group1"> <img class="header-content chevrons" id="chevron1" src="images/chevron.png" height=150px width=150px/></a>
    <a href="#group1"> <img class="header-content chevrons" id="chevron2" src="images/chevron.png" height=150px width=150px/></a>
    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/dz8S7x50bSE?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

  </header>

  <div id="group1">

    <div id="intro-video" class="group-item">
      <img id="team-photo" src="images/team.jpg"/>
      <!-- <iframe src="https://www.youtube.com/embed/dz8S7x50bSE?autoplay=0&amp;amp;rel=0&amp;amp;showinfo=0" width="1280" height="720" frameborder="0" allowfullscreen=""></iframe> -->
      <p class="media-description">L'équipe derrière cette réalisation</p>
    </div>

    <div id="news" class="group-item">
      <h1>Dernières nouvelles</h1>

      <!-- <div id="headcase" class="case"> -->
      <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FVieuxRobots%2F&tabs=timeline&width=340&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
      <!-- </div> -->

    </div>




  </div>

  <section id="ecole" class="banner">

    <h1>Le cégep du Vieux-Montréal</h1>

    <p>Le cégep du Vieux Montréal a été fondé en 1968 par le regroupement d'écoles de renom au coeur du Quartier Latin :</p>
<ul>
  <li>Institut des arts appliqués</li>
  <li>Mont St-Louis</li>
  <li>Institut de Technologie de Montréal</li>
  <li>Collège Ste-Marie</li>
  <li>École des arts graphiques</li>
  <li>Écoles d'infirmières des hôpitaux du centre-ville</li>

</ul>
<p>Aujourd'hui, le CVM est un établissement d'enseignement collégial offrant un éventail de 52 programmes, profils et options de formation préuniversitaires et techniques qui eux-mêmes rassemblent 6100 étudiants.</p>

<p>Le cégep du Vieux Montréal vise la formation de personnes compétentes, autonomes, cultivées et engagées dans leur milieu, tout en valorisant le soutien et l'aide à la réussite à travers une approche humaniste de la formation. Il propose le ralliement d'une communauté engagée autour de projets communs. Bref, il met toutes ses ressources en œuvre pour favoriser le développement intégral de la personne.</p>

<p>Plus qu'un centre de formation, le Cégep est aussi un milieu de vie dynamique. Chaque étudiant peut parfaire ses acquis et s'impliquer dans son milieu grâce à une vaste gamme d'activités sociales, culturelles, artistiques et scientifiques.
</p>

<p>C'est par l'énergie et la créativité estudiantines que vibre le cégep du Vieux Montréal.</p>
<a href="http://www.cvm.qc.ca/cegep/apropos/Pages/index.aspx">http://www.cvm.qc.ca/cegep/apropos/Pages/index.aspx</a>


  </section>


</body>


</html>
