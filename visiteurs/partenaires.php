
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Partenaires - Les Vieux-Robots</title>
	<link href="css/global.css" rel="stylesheet" type="text/css" />
	<link href="css/nav.css" rel="stylesheet" type="text/css" />
	<link href="css/partenaires.css" rel="stylesheet" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>


<?php

	// ALLER CHERCHER LE NAV DU SITE
	require_once("partial/nav.php");

 ?>


		<header>
			<div class="buildspace"></div>
			<h1>Nos partenaires</h1>
		</header>

		<main>

			<p>Voici nos commanditaires, sans qui notre participation telle qu'elle est serait "supposément" impossible.</p>

			<div class="partenaires">
				<div class="partenaire">
					<img src="images/commandite/bcgo.jpg">
				</div>

				<div class="partenaire">
					<img src="images/commandite/champigny.jpg">
				</div>

				<div class="partenaire">
					<img src="images/commandite/gjm.jpg">
				</div>

				<div class="partenaire">
					<img src="images/commandite/itabec.jpg">
				</div>
			</div>



		</main>

		<?php

			// ALLER CHERCHER LE CONTENU DE L'EN-TÊTE DU SITE
			require_once("partial/footer.php");

		 ?>
